import curses
import random
import world
from world import Position
from world import Tile
from world import Symbol
from world import Object
from world import Map
from world import RectangleArea
from world import MapEvent

# Define the colors
BLACK = 0
BLUE = 1
CYAN = 2
GREEN = 3
MAGENTA = 4
RED = 5
WHITE = 6
YELLOW = 7

#Possible directions (points of the compass).
NORTH   = Position(0,-1)
SOUTH   = Position(0,1)
EAST    = Position(1,0)
WEST    = Position(-1,0)
NORTH_E = NORTH + EAST
NORTH_W = NORTH + WEST
SOUTH_E = SOUTH + EAST
SOUTH_W = SOUTH + WEST

# Maybe we should add keys for the intermediary points of the compass.
directions = {	curses.KEY_LEFT:  WEST,
		curses.KEY_RIGHT: EAST,
	 	curses.KEY_UP:    NORTH,
		curses.KEY_DOWN:  SOUTH }

# Symbols for different
EMPTY    = Symbol(' ', BLACK,  ' ')
WATER    = Symbol('=', BLUE,   '=')
LAND     = Symbol(':', GREEN,  ':')
MOUNTAIN = Symbol('^', WHITE,  '^')
FOREST   = Symbol('&', GREEN,  '&')
FIELD    = Symbol('#', YELLOW, '#')
DESERT   = Symbol('.', YELLOW, '.')
PLAYER   = Symbol('@', WHITE,  '@')

# FIXME/tuos: This is just a quick hack.
class Param:
	def __init__(self, symbol, size, count):
		self.symbol = symbol
		self.size = size
		self.count = count

# Initially generated symbols.
# Add new: Add symbol into the list below.
# First parameter tells how many lands, mountain ranges etc.
# Second tells how big they are going to be.
INITIAL_MAP_PARAMS = [	Param(LAND, 15, 30),
			Param(MOUNTAIN, 10, 20),
			Param(FOREST, 10, 10)]

# The player viewport size
PLAY_AREA_ROWS = 10
PLAY_AREA_COLS = 40

worldmap = Map(160, 40, WATER)

player = Object(Position(2,2), PLAYER, "Player.")

def create_tile(symbol, position, description, size):
	if size < 0:
		return
	if not worldmap.has_position(position):
		return
	size -= 1
	try:
		worldmap.insert(Tile(position, symbol, description))
	except:
		return
	create_tile(symbol, position + NORTH, description, size)
	create_tile(symbol, position + SOUTH, description, size)
	create_tile(symbol, position + WEST, description, size)
	create_tile(symbol, position + EAST, description, size)

def generate_world():
	"""Generates the world."""
	for param in INITIAL_MAP_PARAMS:
		for i in range(param.count):
			x = random.randint(0, worldmap.width() -1 )
			y = random.randint(0, worldmap.height() - 1)
			create_tile(param.symbol, Position(x, y), "Symbol description.", param.size)

class Mapview(RectangleArea):

	def __init__(self, width, height, stdscr):
		RectangleArea.__init__(self, width, height)
		self.__width_offset__ = 0
		self.__height_offset__ = 0
		self.__stdscr__ = stdscr

	def map_event(self, event):
		self.__stdscr__.addstr(20, 20, "Unknown command.")
		self.__stdscr__.refresh()
		if event.type() == MapEvent.AFTER_INSERT:
			self.draw_object(event.object())
	
	def draw_object(self, object):
		char = object.symbol().char()
		color = object.symbol().color()
		x = object.position().x() - self.__width_offset__
		y = object.position().y() - self.__height_offset__
		pos = Position(x,y)
		if self.has_position(pos):
			self.__stdscr__.addstr(y,x, char, curses.color_pair(color))


# FIXME/tuos: This method should be a part of screen/vieport
def draw_map(stdscr, player):
	"""Draws the map and the player to the screen."""

	minrow = player.position().y() - PLAY_AREA_ROWS
	maxrow = player.position().y() + PLAY_AREA_ROWS
	mincol = player.position().x() - PLAY_AREA_COLS
	maxcol = player.position().x() + PLAY_AREA_COLS

	player_row_correction = 0
	player_col_correction = 0
	if minrow < 0:
		player_row_correction = minrow
		minrow = 0
		maxrow = PLAY_AREA_ROWS*2
	if worldmap.height() < maxrow:
		player_row_correction = - (worldmap.height() - maxrow)
		minrow = worldmap.height()
		maxrow = worldmap.height()
	if mincol < 0:
		player_col_correction = mincol
		mincol = 0
		maxcol = PLAY_AREA_COLS*2
	if worldmap.width() < maxcol:
		player_col_correction = -(worldmap.width() - maxcol)
		mincol = worldmap.width() - (PLAY_AREA_COLS*2)
		maxcol = worldmap.width()
	rown = 0

	for row in range(minrow,maxrow):
		coln = 0
		for col in range(mincol,maxcol):
			tile = worldmap.get(Position(col, row))
			char = tile.symbol().char()
			color = tile.symbol().color()
			stdscr.addch(rown, coln, char, curses.color_pair(color))
			coln += 1
		rown += 1

	stdscr.addstr(PLAY_AREA_ROWS + player_row_correction, PLAY_AREA_COLS + player_col_correction, "@", curses.color_pair(WHITE))

# FIXME/tuos: This method should be a part of screen/vieport
def init_curses():
	curses.curs_set(0)
	curses.init_pair(BLUE, curses.COLOR_BLUE, curses.COLOR_BLACK)
	curses.init_pair(CYAN, curses.COLOR_CYAN, curses.COLOR_BLACK)
	curses.init_pair(GREEN, curses.COLOR_GREEN, curses.COLOR_BLACK)
	curses.init_pair(MAGENTA, curses.COLOR_MAGENTA, curses.COLOR_BLACK)
	curses.init_pair(RED, curses.COLOR_RED, curses.COLOR_BLACK)
	curses.init_pair(WHITE, curses.COLOR_WHITE, curses.COLOR_BLACK)
	curses.init_pair(YELLOW, curses.COLOR_YELLOW, curses.COLOR_BLACK)

def main(stdscr):
	"""Asks for input and executes commands until game ends."""
	init_curses()
	random.seed()
#	mv = Mapview(PLAY_AREA_COLS, PLAY_AREA_ROWS, stdscr)
	generate_world()
#	worldmap.add_observer(mv)
#	worldmap.remove_observer(mv)
	end = False
	while not end:
		stdscr.clear()
		draw_map(stdscr, player)
#		mv.draw_object(player)
		c = stdscr.getch()
		if c == ord('q'):
			end = True
			continue
		try:
			commands[c]()
		except:
			stdscr.addstr(20, 20, "Unknown command.")
		player.move(directions[c])

if __name__ == "__main__":
	curses.wrapper(main)

