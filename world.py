import math

class Position:
	"""
	Simple class for position information. Implements 'comparable
	interface' by implementing __cmp__ -method. Comparing is based purely
	on distances between two Position-instances.
	"""
	def __init__(self, x, y):
		self.__x__ = x
		self.__y__ = y

	def x(self):
		"""
		>>> pos = Position(3,4)
		>>> pos.x()
		3
		"""
		return self.__x__

	def y(self):
		"""
		>>> pos = Position(5,6)
		>>> pos.y()
		6
		"""
		return self.__y__

	def __add__(self, other):
		return Position(self.x() + other.x(), self.y() + other.y())

	def __sub__(self, other):
		return Position(self.x() - other.x(), self.y() - other.y())

	def distance(self, other):
		"""
		Counts the distance between self and other.

		Examples:
		>>> origin = Position(0,0)
		>>> origin.distance(Position(0,0))
		0.0
		>>> origin.distance(Position(5,7))
		8.6023252670426267
		>>> origin.distance(origin)
		0.0
		>>> pos = Position(2,4)
		>>> pos.distance(Position(6,7))
		5.0
		"""
		delta_x = self.x() - other.x()
		delta_y = self.y() - other.y()
		return math.sqrt(math.pow(delta_x, 2) + math.pow(delta_y, 2))

	def __cmp__(self, other):
		"""
		>>> origin = Position(0,0)
		>>> origin == origin
		True
		>>> origin == Position(0,0)
		True
		>>> origin <= (origin)
		True
		>>> origin >= (origin)
		True
		>>> origin != (origin)
		False
		>>> origin < Position(3,4)
		True
		>>> Position(5,5) > Position(4,1)
		True
		"""
		origin = Position(0,0)
		return self.distance(origin) - other.distance(origin)
		
class Symbol:
	"""
	Symbolic representation for the use of different objects. Every symbol
	contains char+color -combination the be represented in the maps of the
	game. Every symbol can be also referenced by an unique_char. At the
	moment, it's up to the developer to take care of uniqueness of these
	unique_chars. It's also up to the developer to take care of the
	relationship between unique_char and char+color -combination.
	"""

	def __init__(self, char, color, unique_char):
		self.__char__ = char
		self.__color__ = color
		self.__unique_char__ = unique_char

	def char(self):
		return self.__char__

	def color(self):
		return self.__color__

	def unique_char(self):
		return self.__unique_char__

	def __eq__(self, other):
		"""
		>>> Symbol('^', 7, 'a') == Symbol('!', 3, 'a')
		True
		>>> Symbol('=', 1, 'b') == Symbol('=', 2, 'c')
		False
		>>> Symbol('=', 1, 'b') == Symbol('=', 1, 'c')
		False
		"""
		return self.unique_char() == other.unique_char()

	def __str__(self):
		return self.char()
	
class Object:
	"""
	Abstract baseclass for every object in the game. At minimum, every
	object has a position information, a symbolic (color + char)
	representation and a description which reveals it's deepest essence to
	the players.
	"""
	def __init__(self, position, symbol, description):
		self.__pos__ = position
		self.__sym__ = symbol
		self.__desc__ = description

	def move(self, direction):
		"""
		Parameter direction is a kind of direction vector, still being
		instance of Position-class. It represents the new relative
		position of this object.

		Example:
		>>> o = Object(Position(4,5),None, None)
		>>> o.move(Position(0,1)) # North
		>>> Position(4,6) == o.position() # Assume 'speed' == 1
		True
		"""
		self.__pos__ = self.position() + direction

	def position(self):
		return self.__pos__

	def symbol(self):
		return self.__sym__

	def description(self):
		return self.__desc__

	def __str__(self):
		return self.symbol().__str__()


class Tile(Object):
	def __init__(self, position, symbol, description=""):
		Object.__init__(self, position, symbol, description)


class WorldException(Exception):
	"""
	Baseclass for every exception that Map-class may throw.
	"""
	pass


class RectangleArea:
	def __init__(self, width, height):
		self.__height__ = height
		self.__width__ = width

	def surfacearea(self):
		"""
		>>> Map(4,5,None).surfacearea()
		20
		"""
		return self.width() * self.height()

	def has_position(self, pos):
		"""
		>>> m = Map(5,5, None)
		>>> m.has_position(Position(3,3))
		True
		>>> m.has_position(Position(6,1))
		False
		>>> m.has_position(Position(5,5))
		False
		"""
		return pos.x() < self.width() and pos.y() < self.height()

	def height(self):
		return self.__height__

	def width(self):
		return self.__width__

	def __position__(self, i):
		y = i / self.width()
		x = i % self.width()
		return Position(x,y)

	def __index__(self, pos):
		return pos.y() * self.width() + pos.x()

class StoringRectangleArea(RectangleArea):
	def __init__(self, width, height, empty_symbol):
		RectangleArea.__init__(self, width, height)
		self.__empty_symbol__ = empty_symbol
		self.__items__ = []
		self.__init_items__()

	def __init_items__(self):
		for i in range(self.surfacearea()):
			pos = self.__position__(i)
			t = Tile(pos, self.empty_symbol(), "Space. Cold.")
			self.__items__.append(t)


	def is_complete(self):
		"""
		Returns a boolean value that indicates whether every position
		in the area is populated by a item. If there is even one
		empty_symbol, this method returns false.

		Examples:

		>>> import world
		>>> WATER =  world.Symbol('=', 1, 'a')
		>>> DESERT = world.Symbol('.', 3, 'b')
		>>> area = Map(2,2,None)
		
		"""
		for item in self:
			if item.symbol() == self.empty_symbol():
				return False
		return True
	
	def reserved(self, pos):
		item = self.get(pos)
		if item.symbol() != self.empty_symbol():
			return True
		return False

	def empty_symbol(self):
		return self.__empty_symbol__

	def get(self, pos):
		i = self.__index__(pos)
		return self.__items__[i]

	def __iter__(self):
		return self.__items__.__iter__()

#	def fromlist(self, tiles, replace=False):
#		"""
#		Inserts tiles from the given list.
#		"""
#		if len(tiles) != self.surfacearea():
#			raise WorldException("Data does not fit into the map.")
#		raise WorldException("Not yet implemented.")
		
	def insert(self, tile, replace=False):
		"""
		Public interface for inserting tiles into the map.
		Overrides Map's __insert-method and adds handling for replacing
		inserts. Throws WorldException if there is already a tile in
		position (x,y) and replacing is not allowed. By default,
		replacing is forbidden.
		"""
		pos = tile.position()
		if self.reserved(pos) and not replace:
			raise WorldException("Position %s is reserved." % pos)
		i = self.__index__(pos)
		self.__items__[i] = tile


class MapEvent:
	BEFORE_INSERT = 0
	AFTER_INSERT = 1

	def __init__(self, type, object):
		self.__type__ = type
		self.__object__ = object
	
	def type(self):
		return self.__type__

	def object(self):
		return self.__object__


class Map(StoringRectangleArea):
	"""
	Class to represent rectangle-shaped areas populated by different kinds
	of tiles. Tiles are accessed using coordinate-like attributes.
	"""
	def __init__(self, width, height, empty_symbol):
		StoringRectangleArea.__init__(self, width, height, empty_symbol)
		self.__observers__ = set()

	def add_observer(self, observer):
		self.__observers__.add(observer)

	def remove_observer(self, observer):
		self.__observers__.discard(observer)

	def notify_observers(self, event):
		for observer in self.__observers__:
			observer.map_event(event)
		
	def insert(self, tile, replace=False):
		"""
		Public interface for inserting tiles into the map.
		Overrides Map's __insert-method and adds handling for replacing
		inserts. Throws WorldException if there is already a tile in
		position (x,y) and replacing is not allowed. By default,
		replacing is forbidden.
		"""
		self.notify_observers(MapEvent(MapEvent.BEFORE_INSERT, tile))
		StoringRectangleArea.insert(self, tile, replace)
		self.notify_observers(MapEvent(MapEvent.AFTER_INSERT, tile))

def _test():
	import doctest
	doctest.testmod()

if __name__ == "__main__":
	_test()			
